import api from "../apis/api";

export const addTransaction = (
  amount,
  category,
  description
) => async dispatch => {
  const response = await api.post("/transactions", {
    amount,
    category,
    description
  });
  if (response.status === 201) {
    dispatch({ type: "ADD_TRANSACTION", payload: response.data });
  }
};

export const amountChange = amount => ({
  type: "AMOUNT_INSERTED",
  amount
});

export const minusOrPlus = selectButtonValue => ({
  type: "TRANSACTION_BUTTON_VALUE",
  selectButtonValue
});

export const buttonSelect = selectButton => ({
  type: "TRANSACTION_BUTTON_SELECTED",
  selectButton
});

export const selectCategory = category => ({
  type: "CATEGORY_SELECTED",
  category
});

export const fetchTransactions = () => async dispatch => {
  const response = await api.get("/transactions");
  dispatch({ type: "FETCH_TRANSACTIONS", payload: response.data });
};

export const fetchCategories = () => async dispatch => {
  const response = await api.get("/category");
  dispatch({ type: "FETCH_CATEGORIES", payload: response.data });
};
