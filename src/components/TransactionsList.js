import React from "react";
import { connect } from "react-redux";
import {
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from "shards-react";
import { fetchTransactions } from "../actions";

class TransactionsList extends React.Component {
  componentDidMount() {
    this.props.fetchTransactions();
  }

  render() {
    return (
      <ListGroup>
        {this.props.transactionList.map(transaction => (
          <ListGroupItem key={transaction._id}>
            <ListGroupItemHeading>{transaction.amount}</ListGroupItemHeading>
            <div className="currency">
              <b>$HKD</b>
            </div>
            <ListGroupItemText>
              Category: <br></br>
              <span>{transaction.category}</span>
            </ListGroupItemText>
          </ListGroupItem>
        ))}
      </ListGroup>
    );
  }
}

const mapStateToProps = state => {
  return {
    transactionList: state.transactionList
  };
};

export default connect(mapStateToProps, { fetchTransactions })(
  TransactionsList
);
