import React from "react";
import { connect } from "react-redux";
import { Badge } from "shards-react";
import { fetchCategories } from "../actions";

class CategoryList extends React.Component {

  componentDidMount() {
    this.props.fetchCategories()
  }

  handleCategoryEvent = e => {
    this.props.handleCategory(e.target.id);
  };
  render() {
    return (
      <div className="categories">
        {this.props.categoryList.map(category => {
          return (
            <Badge key={category._id} theme={category.theme} id={category.name} onClick={this.handleCategoryEvent} className={category.name === this.props.selectCategory ? 'selected' : null}>
              {category.name}
            </Badge>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    categoryList: state.categoryList,
    selectCategory: state.selectCategory
  };
};

export default connect(mapStateToProps, { fetchCategories })(CategoryList);
