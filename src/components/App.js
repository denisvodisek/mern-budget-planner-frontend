import React from "react";
import TransactionForm from "./TransactionForm";
import TransactionsList from "./TransactionsList";
import Menu from "./Menu";

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";

import "../assets/css/main.css";
import { Container, Row, Col } from "shards-react";

function App() {
  return (
    <div className="App">
      <Menu />
      <Container>
        <Row>
          <Col>
          <TransactionForm></TransactionForm>
          </Col>
        </Row>
        <Row>
        <Col>
      <TransactionsList />
      </Col>
      </Row>
      </Container>
    </div>
  );
}

export default App;
