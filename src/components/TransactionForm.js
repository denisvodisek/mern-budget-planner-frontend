import React from "react";
import CategoryList from "./CategoryList";
import {
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput
} from "shards-react";
import { connect } from "react-redux";
import {
  addTransaction,
  amountChange,
  minusOrPlus,
  selectCategory,
  buttonSelect,
} from "../actions";
import { CSSTransition } from "react-transition-group";

class TransactionForm extends React.Component {
  handleChange = e => {
    this.props.amountChange(e.target.value);
  };

  handleSubmit = e => {
    e.preventDefault();
    if (!this.props.amount) return;
    console.log(this.props.buttonSelected);
    if (this.props.buttonSelected) {
      this.props.addTransaction(
        parseFloat(this.props.buttonValue + this.props.amount),
        this.props.category
      );
      this.props.amountChange("");
      this.props.buttonSelect(false);
      this.props.minusOrPlus("");
    }
  };

  handleClick = e => {
    if (this.props.buttonValue) {
      this.props.minusOrPlus("");
      this.props.buttonSelect(false);
    } else {
      this.props.buttonSelect(true);
      this.props.minusOrPlus(e.target.id);
    }
  };

  handleCategory = (e, id) => {
    this.props.selectCategory(e);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="transactionForm">
          <div className="buttons">
            {this.props.buttonValue !== "-" && (
              <Button outline theme="success" id="+" onClick={this.handleClick}>
                +
              </Button>
            )}
            {this.props.buttonValue !== "+" && (
              <Button outline theme="danger" id="-" onClick={this.handleClick}>
                -
              </Button>
            )}
          </div>
          <CSSTransition
            in={this.props.buttonSelected}
            classNames="transition"
            timeout={300}
            unmountOnExit
            onExited={() => this.props.buttonSelect(false)}
          >
            <div className="input">
              <CategoryList handleCategory={this.handleCategory} />
              <InputGroup>
                <InputGroupAddon type="prepend">
                  <InputGroupText>$HKD</InputGroupText>
                </InputGroupAddon>
                <FormInput
                  placeholder="Amount"
                  type="number"
                  value={this.props.amount}
                  onChange={this.handleChange}
                />
                <Button
                  outline
                  theme="secondary"
                  id="-"
                  onClick={this.handleSubmit}
                >
                  OK
                </Button>
              </InputGroup>
            </div>
          </CSSTransition>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    amount: state.amount,
    buttonSelected: state.selectButton,
    buttonValue: state.selectButtonValue,
    category: state.selectCategory
  };
};

export default connect(mapStateToProps, {
  addTransaction,
  amountChange,
  minusOrPlus,
  buttonSelect,
  selectCategory
})(TransactionForm);
