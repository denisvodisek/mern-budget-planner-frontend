const categoryList = (categoryList = [], action) => {
  switch (action.type) {
    case "FETCH_CATEGORIES":
      return action.payload;
    default:
      return categoryList;
  }
};

export default categoryList;
