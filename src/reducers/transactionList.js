const transactionList = (transactionList = [], action) => {
  switch (action.type) {
    case "ADD_TRANSACTION":
      return [action.payload, ...transactionList];
    case "FETCH_TRANSACTIONS":
      return action.payload;
    default:
      return transactionList;
  }
};

export default transactionList;
