export const amount = (amount = 0, action) => {
  switch (action.type) {
    case "AMOUNT_INSERTED":
      return action.amount;
    default:
      return amount;
  }
};

export const selectButtonValue = (button = "", action) => {
  switch (action.type) {
    case "TRANSACTION_BUTTON_VALUE":
      return action.selectButtonValue;
    default:
      return button;
  }
};

export const selectButton = (state = false, action) => {
  switch (action.type) {
    case "TRANSACTION_BUTTON_SELECTED":
      return action.selectButton;
    default:
      return state;
  }
};

export const selectCategory = (category = "Other", action) => {
  switch (action.type) {
    case "CATEGORY_SELECTED":
      return action.category;
    default:
      return category;
  }
};
