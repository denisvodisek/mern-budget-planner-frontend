import { combineReducers } from "redux";
import { amount, selectButton, selectCategory, selectButtonValue } from "./transactions";
import transactionList from "./transactionList";
import categoryList from "./categoryList";
export default combineReducers({
  amount,
  selectButton,
  selectCategory,
  selectButtonValue,
  transactionList,
  categoryList
});
